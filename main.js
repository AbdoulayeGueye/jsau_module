'use strict'

let modules = ["sante", "culture", "social", "enseignement", "sport"]
function validate (cats){
    return modules.includes(cats)
}

module.exports={
    validate,
    categories
}